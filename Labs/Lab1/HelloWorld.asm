#Program starts with imported libraries and packages.
.data
hello:
.asciiz
"Hello world!\n"
.text
#Saving the memory address associated with hello world to the variable $a0 
la $a0, hello
#It immediatly loads the number 4 to the variable $v0
li $v0, 1
#Calls syscall 4
syscall
#Immediately loads the number 10 to the variable $v0, which changes the syscall to end the program
li $v0, 10
syscall
