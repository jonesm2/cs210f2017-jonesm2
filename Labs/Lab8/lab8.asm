.data
	list:	  	.space		1000
	listsz:   		.word		250
	String:		.asciiz		"Please Enter an even interger for the Length of the array"
	StringA:		.asciiz		"ArrayP ="
	StringB:		.asciiz		"ArrayQ ="
	NewLine:		.asciiz		"\n"
	Comma:		.asciiz		", "
.text
	
	# Prompt the user for input
	li $v0, 4
	la $a0, String
	syscall
	
	# Getting input from user
	li $v0, 5
	syscall
	
	# Move input to $t4, sets arraySize = user input
	move $t4, $v0
	
	#Sets counter $t0, and halfway point $t1, $s1 = array addr, $s0 = array dim
	li $t0, 0
	li $t2, 2
	div $t4, $t2
	mflo $t1
	lw $s0, listsz
#First Array
	#Communicates to user which Array is being printed
	li $v0, 4
	la $a0, StringA
	syscall
	subi $t5, $t4, 1 
	#Fills array, branches to the 2nd array once complete
     FILL:  beq $t0, $t4, ARRAY2
	addi $t0, $t0, 1
	
	li $v0, 1		#Print statement
	move $a0, $t0		#"
	syscall 		#"
	li $v0, 4
	la $a0, Comma
	syscall
	j FILL
# Second Array
  ARRAY2:
	#Communicates to user which Array is being printed
	li $v0, 4
	la $a0, NewLine
	syscall
	la $a0, StringB
	syscall
	#Fills first half of the list
	addi $t0, $zero, 0	#Reset counter
        FIRST:
	beq $t0, $t1, SECOND
	addi $t0, $t0, 1
	mult $t0, $t2
	mflo $t3
	move $s1, $t3
	li $v0, 1		#Print statement
	move $a0, $s1		#"
	syscall 		#"
	li $v0, 4
	la $a0, Comma
	syscall
	j FIRST
	#Fills 2nd half of the list
SECOND:
	li $t0, 0
SECONDL:
  	beq $t0, $t4, EXIT
  	addi $s1, $s1, 1
  	addi $t0, $t0, 1
  	
  	li $v0, 1		#Print statement
	move $a0, $s1		#"
	syscall 		#"
	li $v0, 4
	la $a0, Comma
	syscall
	j SECONDL
  	
    EXIT: 
    	li $v0, 10
	syscall
