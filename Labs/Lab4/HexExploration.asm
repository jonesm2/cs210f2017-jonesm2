.data
    hello:      .asciiz     "Hello world!\n"
    number1:    .word       42
    number2:    .half       21
    number3:    .half       1701
    #number4:    .byte       73
    number5:    .half       -1701
    number6:    .byte       127
    number7:    .word       65536
    
    #######################
    #  ADDRESS   #  DATA  #
    #######################
    # 0x10010010 #        #
    # 0x10010011 #        #
    # 0x10010012 #        #
    # 0x10010013 #        #
    # 0x10010014 #        #
    # 0x10010015 #        #
    # 0x10010016 #        #
    # 0x10010017 #        #
    # 0x10010018 #        #
    # 0x10010019 #        #
    # 0x1001001a #        #
    # 0x1001001b #        #
    # 0x1001001c #        #
    # 0x1001001d #        #
    # 0x1001001e #        #
    # 0x1001001f #        #
    # 0x10010020 #number7        #
    # 0x10010021 #        #
    # 0x10010022 #        #
    # 0x10010023 #        #
    # 0x10010024 #        #
    # 0x10010025 #        #
    # 0x10010026 #        #
    # 0x10010027 #        #
    #######################


.text

    la $a0, hello
    li $v0, 4
    syscall
    
    #la $t0 number1
    #la $t6 number6
    #la $t7 number7
    
   #lw $t8 0($t0)
    #lh $t9 0($t1)
    li $v0, 10
    syscall
