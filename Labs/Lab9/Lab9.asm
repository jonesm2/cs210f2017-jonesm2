.data
	String1:	.asciiz	"Welcome to this Guessing Game, human!\nI am thinking of a number between 1 and 100.\nSee if you can guess it!"	
	String2:	.asciiz	"\nYour guess was too high (Enter a # to Cont.)."
	String3:	.asciiz "\nYour guess was too low (Enter a # to Cont..)."
	String4:	.asciiz	"\nYour guess was: "
	String5:	.asciiz "\nYou got it in "
	String6:	.asciiz	" tries.\nMy number was "
	String7:	.asciiz	"\nWould you like to play again?(Y/N)"
	 buffer: .space 20
   	 yes: .asciiz "Y"
.text
Start:
	la $s7, Start	#loading register for the again procedure to jump to
	#generating random number
	li $s0, 0
	li $v0, 42  # 42 is system call code to generate random int
	li $a1, 100 # $a1 is where you set the upper bound
	syscall
	move $s1, $a0
	jal Intro
loop:			#main loop which sets up procedures
	jal UserInput
	jal Compare
	beq $s2, $s1, Equal	#if the two values are equal, it jumps the loop to the equal procedure call
	j loop
Equal:	jal equal
	jal Again 
Intro:
	# prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
	
	# procedure body
	li $v0, 4
	la $a0, String1
	syscall
	
	#Epilogue
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra
UserInput:	
	# prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
	
	# procedure body
	li $v0, 5
	syscall
	move $s2, $v0
	
	#Epilogue
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra
Compare:
	# prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
		
	# procedure body	
	blt $s2, $s1, Less
	bgt $s2, $s1, Greater
	jr $ra
	
Less:
	li $v0, 4
	la $a0, String4
	syscall
	li $v0, 1
	move $a0, $s2		
	syscall
	li $v0, 4
	la $a0, String3
	syscall
	addi $s0, $s0, 1
	
	#Epilogue
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra	
Greater:
	li $v0, 4
	la $a0, String4
	syscall
	li $v0, 1
	move $a0, $s2		
	syscall
	li $v0, 4
	la $a0, String2
	syscall
	addi $s0, $s0, 1
	
	#Epilogue
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra
equal:
	addi $s0, $s0, 1
	li $v0, 4
	la $a0, String4
	syscall
	li $v0, 1
	move $a0, $s2		
	syscall
	li $v0, 4
	la $a0, String5
	syscall
	li $v0, 1
	move $a0, $s0		
	syscall
	li $v0, 4
	la $a0, String6
	syscall
	li $v0, 1
	move $a0, $s2
	syscall
Again:	
	# prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
	
	# procedure body
	li $v0, 4
	la $a0, String7
	syscall
	li $v0,8 #take in input
        la $a0, buffer #load byte space into address
        li $a1, 20 # allot the byte space for string
        move $s3,$a0
        syscall
       	lb $s4, yes
      	lb $s5, 0($s3)
      	beq $s4, $s5, again
	j Exit
again:	
	#Epilogue
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	jr $s7 
    Exit:
    	li $v0 10
    	syscall
