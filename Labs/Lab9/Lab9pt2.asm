.data
	String1:	.asciiz	"Welcome to this Guessing Game, human!\nThink of a number between 1 and 100.\nI'll see if I can guess it!"	
	String2:	.asciiz	"\nMy guess was too high."
	String3:	.asciiz "\nMy guess was too low."
	String4:	.asciiz	"\nMy guess is: "
	String8:  .asciiz " How did I do? (-1, 0, 1)"
	String5:	.asciiz "\nYay! I got it in "
	String6:	.asciiz	" tries.\nYour number was "
	String7:	.asciiz	"\nWould you like to play again?(Y/N)"
	 buffer: .space 20
   	 yes: .asciiz "Y"
.text
Start:
	la $s7, Start	#loading register for the again procedure to jump to
	li $s0, 0
	li $s3, 1
	li $s4, -1
	li $v0, 4
	la $a0, String1
	syscall
	li $v0, 5
	syscall
	move $s1, $v0
	li $s2, 50
loop:			#main loop which sets up procedures
	jal Compare
	jal UserInput
	beq $s2, $s1, Equal	#if the two values are equal, it jumps the loop to the equal procedure call
	j loop
Equal:	jal equal
	jal Again
Compare:
	# prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
		
	# procedure body	
	li $v0, 4
	la $a0, String4
	syscall
	li $v0, 1
	move $a0, $s2
	syscall
	li $v0, 4
	la $a0, String8
	syscall
	
	#Epilogue
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
	jr $ra

UserInput:	
	# prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
	
	# procedure body
	li $v0, 5
	syscall
	move $s5, $v0
	beq $s5, $s3, Less
	beq $s5, $s4, Greater
	#Epilogue
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra
Greater: 
	# prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
	
	# procedure body
	li $v0, 4
	la $a0, String3
	syscall
	addi $s2, $s2, 1
	addi $s0, $s0, 1
	
	#Epilogue
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra
Less:
	# prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
	
	# procedure body
	li $v0, 4
	la $a0, String3
	syscall
	subi $s2, $s2, 1
	addi $s0, $s0, 1
	
	#Epilogue
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra
equal:
	addi $s0, $s0, 1
	li $v0, 4
	la $a0, String4
	syscall
	li $v0, 1
	move $a0, $s2
	syscall
	li $v0, 4
	la $a0, String5
	syscall
	li $v0, 1
	move $a0, $s0		
	syscall
	li $v0, 4
	la $a0, String6
	syscall
	li $v0, 1
	move $a0, $s2
	syscall
Again:	
	# prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
	
	# procedure body
	li $v0, 4
	la $a0, String7
	syscall
	li $v0,8 #take in input
        la $a0, buffer #load byte space into address
        li $a1, 20 # allot the byte space for string
        move $s3,$a0
        syscall
       	lb $s4, yes
      	lb $s5, 0($s3)
      	beq $s4, $s5, again
	j Exit
again:	
	#Epilogue
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	jr $s7 
    Exit:
    	li $v0 10
    	syscall